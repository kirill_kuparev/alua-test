import React, {Component} from 'react';
import axios from 'axios';

import Preview from '../components/Preview';
import Header from "../components/Header";
import InfiniteScroll from 'react-infinite-scroll-component';
import Loading from '../components/Loading';
import cash from '../components/cash';

export default class homePage extends Component {

    constructor(props) {
        super(props);
        let data = cash.saveList([]);
        this.state = {
            data,
            isLoading: true,
            pagination: {
                offset: 0,
                limit: 10,
                total: 0,
            },
        };
    }

    componentDidMount() {
        this.getData();
    }

    load() {
        this.setState((prevState) => ({
            offset: prevState.offset + prevState.limit,
        }), this.getData);
    }

    getData(val = false) {
        this.setState({isLoading: true});
        const pagination = val ? {offset: 0, limit: 10,} : this.state.pagination;
        axios.get(`https://api.alua.com/v1/users/discover?offset=${pagination.offset}&limit=${pagination.limit}`).then((result) => {
            let newData = result.data;
            let users = cash.saveList(result.data.data);
            this.setState(prevState => {
                let {pagination} = prevState;
                pagination.offset += pagination.limit;
                pagination.total = newData.total;
                return {isLoading: false, pagination, data: users};
            });
        });
    }

    render() {
        const {data, pagination} = this.state;

        return (
            <div style={{overflow: 'auto',maxWidth:'600px',margin:'auto'}}>
                <Header/>
                {data && data.length > 0 ?
                <InfiniteScroll
                    className='infinity'
                    hasMore={data.length < pagination.total}
                    next={() => this.load()}
                    loader={<div className='loading'><Loading/></div>}
                    >
                    {data.map((item)=><Preview item={item} className='preview-content'/>)}
                </InfiniteScroll>:<div className='loading'><Loading/></div>}
            </div>

        );
    }

}