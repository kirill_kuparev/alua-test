import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';




import homePage from './homePage/homePage'
import showProfile from "./showProfile/showProfile";




const AppRoute = ({ component: Component, layout: Layout, ...rest }) => (
    <Route {...rest} render={props => (
        <Layout>
            <Component {...props} />
        </Layout>
    )} />
);


const MainLayout = props => (
    <div>
        {props.children}
    </div>
)



class App extends React.Component {

    componentDidMount() {

    }

    render() {
        return (
            <div>
            <Switch>
                <AppRoute exact path="/app/" layout={MainLayout} component={homePage} />
                <AppRoute exact path="/app/view" layout={MainLayout} component={showProfile} />
                {<Redirect from="/" to="/app"  />}
            </Switch>

            </div>
        );
    }
}

export default App