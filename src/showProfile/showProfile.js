import React, {Component} from 'react';
import {Row, Col} from 'react-bootstrap';
import Slides from 'react-slick'
import moment from 'moment';


import Header from "../components/Header";

export default class homePage extends Component {

    render() {

        let settings = {
            dots: true,
            infinite: false,
            speed: 500,
            arrows:false,
        };

        let imageDivs = this.props.location.state.item.featured.map(function(imageURL,index){
            return <div key={index} className='image-divs'><img src={ imageURL.th}  /></div>
        });

        let imageTiles = this.props.location.state.item.featured.map(function(imageURL,index){
            return <div className="col-xs-3 image-tiles" key={index} ><img src={ imageURL.th} alt="" /></div>
        });

        const {city, country, bio,username,is_online,last_online}=this.props.location.state.item;

        return (

            <div >
                <Header back={true} username={username}/>
                <Slides ref={c => this.slider = c } {...settings} >
                    {imageDivs}
                </Slides>
                <Row className='show-profile-content'>
                    <Col xs={7}>
                        <div><b>{city}, {country}</b></div>
                        <div><img src='https://cdn4.iconfinder.com/data/icons/Primo_Icons/PNG/64x64/label_blue_hot.png'/>Verified ,<span className='online'><p className='online'>{(is_online && 'Online') || moment(last_online).fromNow()}</p></span></div>
                        <div><b>{bio}</b></div>
                    </Col>
                    <Col xs={4} xsOffset={1}>
                        <Row >
                            <Col xs={3}>
                                5
                            </Col>
                            <Col xs={9}>
                                <Row>
                                    CREDITS
                                </Row>
                                <Row>
                                    /50 charts
                                </Row>
                            </Col>
                        </Row>
                    </Col>
                </Row>


                <div>
                    <div className="row">
                        {imageTiles}
                        <img src='https://cdn2.iconfinder.com/data/icons/social-icons-33/128/Instagram-64.png' style={{padding:'25px'}}/>
                    </div>
                </div>
            </div>
        );


    }


}