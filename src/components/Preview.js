import React, {Component} from 'react';
import {Link} from 'react-router-dom'


export default class Preview extends Component {

    render() {

        const {username, avatar, title, country,is_online,last_online} = this.props.item;


        return (
            <div id="wrapper">
                <Link to={{
                    pathname: '/app/view',
                    state: {
                        item: this.props.item
                    }
                }}
                >
                    <div className='preview' style={{backgroundImage: 'url(' + avatar.th + ')'}}>
                        <p className='name'>{username}</p>
                        <p className='title'>{title}</p>
                        <p className='country'>{country}</p>
                    </div>
                </Link>
            </div>
        );
    }

}