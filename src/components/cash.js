let listUser = [];

const cash = {
  saveList: (arr) => {
    arr.forEach(item => {
      if(!cash.isUniq(item._id)) listUser.push(item);
    });
      cash.cacheList();
    return listUser;
  },
  cacheList: () => {
    let json = JSON.stringify(listUser);
    localStorage.setItem('list', json)
  },
  getFromCahe: () => {
    let json = localStorage.getItem('list');
    return JSON.parse(json);
  },
  isUniq: (_id) => (listUser && listUser.length > 0 && listUser.find(user => _id === user._id )),
};

window.m = cash;
listUser = cash.getFromCahe() || [];
export default cash;
