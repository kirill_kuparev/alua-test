import React, {Component} from 'react';
import { Navbar,Nav, NavItem } from 'react-bootstrap';
import  {Link} from 'react-router-dom'

export default class Header extends Component {

    constructor(props) {
        super(props);
    }


    render() {
        return (
            <div id="wrapper">
                <Navbar inverse collapseOnSelect>
                    <Navbar.Header className="text-center">
                        {this.props.back?
                            <Link to={{
                                pathname: '/app',
                            }} className='back-link'>
                                <img src='https://cdn0.iconfinder.com/data/icons/audio-controls-ui-icons/40/left-32.png'/>
                            </Link>:''}
                        <Navbar.Brand pullCenter>
                            <a href="#brand">{this.props.back?this.props.username:'ALUA'}</a>
                        </Navbar.Brand>
                        <Navbar.Toggle />
                    </Navbar.Header>
                    <Navbar.Collapse>
                        <Nav pullRight>
                            <NavItem eventKey={1} href="#">
                                Test1
                            </NavItem>
                            <NavItem eventKey={2} href="#">
                                Test2
                            </NavItem>
                        </Nav>
                    </Navbar.Collapse>
                </Navbar>
            </div>
        );
    }

}